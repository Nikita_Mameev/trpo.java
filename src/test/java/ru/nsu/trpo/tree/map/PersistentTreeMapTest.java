package ru.nsu.trpo.tree.map;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PersistentTreeMapTest {

    @Test
    public void put() {
        PersistentTreeMap<String, String> empty = new PersistentTreeMap<>();
        PersistentTreeMap<String, String> base = empty
                .put("1", "1")
                .put("2", "2")
                .put("3", "3")
                .put("4", "4");

        PersistentTreeMap<String, String> sut = base.put("5", "5");

        assertEquals(5, sut.size());
        assertEquals("5", sut.get("5"));
    }

    @Test
    public void set() {
        PersistentTreeMap<String, String> empty = new PersistentTreeMap<>();
        PersistentTreeMap<String, String> base = empty
                .put("1", "1")
                .put("2", "2")
                .put("3", "3")
                .put("4", "4");

        PersistentTreeMap<String, String> sut = base.put("1", "5");

        assertEquals(4, sut.size());
        assertEquals("5", sut.get("1"));
    }

    @Test
    public void remove() {
        PersistentTreeMap<String, String> empty = new PersistentTreeMap<>();
        PersistentTreeMap<String, String> base = empty
                .put("1", "1")
                .put("2", "2")
                .put("3", "3")
                .put("4", "4");

        PersistentTreeMap<String, String> sut = base.remove("2");

        assertEquals(3, sut.size());
        assertNull(null, sut.get("2"));
    }
}
