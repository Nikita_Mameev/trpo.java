package ru.nsu.trpo.array.list;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersistentArrayListTest {

    @Test
    public void add() {
        PersistentArrayList<Integer> empty = new PersistentArrayList<>();
        PersistentArrayList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentArrayList<Integer> sut = base.add(5);

        assertEquals(5, sut.size());
        assertEquals(new Integer(5), sut.get(4));
    }

    @Test
    public void addInCenter() {
        PersistentArrayList<Integer> empty = new PersistentArrayList<>();
        PersistentArrayList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentArrayList<Integer> sut = base.add(2, 5);

        assertEquals(5, sut.size());
        assertEquals(new Integer(5), sut.get(2));
    }

    @Test
    public void remove() {
        PersistentArrayList<Integer> empty = new PersistentArrayList<>();
        PersistentArrayList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentArrayList<Integer> sut = base.remove(1);

        assertEquals(3, sut.size());
        assertEquals(new Integer(1), sut.get(0));
        assertEquals(new Integer(3), sut.get(1));
        assertEquals(new Integer(4), sut.get(2));
    }

    @Test
    public void set() {
        PersistentArrayList<Integer> empty = new PersistentArrayList<>();
        PersistentArrayList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentArrayList<Integer> sut = base.set(2, 5);

        assertEquals(4, sut.size());
        assertEquals(new Integer(5), sut.get(2));
    }

    @Test
    public void addString() {
        PersistentArrayList<String> empty = new PersistentArrayList<>();
        PersistentArrayList<String> base = empty
                .add("1")
                .add("2")
                .add("3")
                .add("4");

        PersistentArrayList<String> sut = base.add("5");

        assertEquals(5, sut.size());
        assertEquals("5", sut.get(4));
    }
}
