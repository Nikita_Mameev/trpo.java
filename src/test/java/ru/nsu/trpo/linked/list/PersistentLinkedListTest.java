package ru.nsu.trpo.linked.list;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersistentLinkedListTest {

    @Test
    public void add(){
        PersistentLinkedList<Integer> empty = new PersistentLinkedList<>();
        PersistentLinkedList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentLinkedList<Integer> sut = base.add(5);

        assertEquals(5, sut.size());
        assertEquals(new Integer(5), sut.get(4));
    }

    @Test
    public void addInCenter(){
        PersistentLinkedList<Integer> empty = new PersistentLinkedList<>();
        PersistentLinkedList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentLinkedList<Integer> sut = base.add(2, 5);

        assertEquals(5, sut.size());
        assertEquals(new Integer(5), sut.get(2));
    }

    @Test
    public void remove(){
        PersistentLinkedList<Integer> empty = new PersistentLinkedList<>();
        PersistentLinkedList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentLinkedList<Integer> sut = base.remove(1);

        assertEquals(3, sut.size());
        assertEquals(new Integer(1), sut.get(0));
        assertEquals(new Integer(3), sut.get(1));
        assertEquals(new Integer(4), sut.get(2));
    }

    @Test
    public void set(){
        PersistentLinkedList<Integer> empty = new PersistentLinkedList<>();
        PersistentLinkedList<Integer> base = empty
                .add(1)
                .add(2)
                .add(3)
                .add(4);

        PersistentLinkedList<Integer> sut = base.set(2, 5);

        assertEquals(4, sut.size());
        assertEquals(new Integer(5), sut.get(2));
    }

    @Test
    public void addString(){
        PersistentLinkedList<String> empty = new PersistentLinkedList<>();
        PersistentLinkedList<String> base = empty
                .add("1")
                .add("2")
                .add("3")
                .add("4");

        PersistentLinkedList<String> sut = base.add("5");

        assertEquals(5, sut.size());
        assertEquals("5", sut.get(4));
    }
}
