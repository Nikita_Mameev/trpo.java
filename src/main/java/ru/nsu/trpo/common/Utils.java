package ru.nsu.trpo.common;

import java.util.List;
import java.util.function.Function;

public class Utils {
    /**
     * В упорядоченном списке состояний находит состояние указанной версии (или ближайшей меньшей версии, если точного совпадения нет)
     * Если нет ни состояния с совпадающей версией, ни с меньшей, возвращает null
     */
    public static <T> T findStateOfVersion(List<T> history, int version, Function<T, Integer> versionExtractor) {
        if (history.isEmpty() || version < versionExtractor.apply(history.get(0))) {
            return null;
        }

        int left = 0;
        int right = history.size() - 1;

        if (version >= versionExtractor.apply(history.get(right))) {
            return history.get(right);
        }

        // Иначе бинарным поиском найдём подходящий элемент (т.к. история упорядочена по возрастанию версии)
        while ((right - left) > 1) {
            int middle = (left + right) / 2;
            T middleState = history.get(middle);
            int middleVersion = versionExtractor.apply(middleState);

            if (version == middleVersion) {
                return middleState;
            } else if (version > middleVersion) {
                left = middle;
            } else /* if (version < middleVersion) */ {
                right = middle;
            }
        }

        return (versionExtractor.apply(history.get(right)) == version) ? history.get(right) : history.get(left);
    }
}
