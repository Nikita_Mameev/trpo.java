package ru.nsu.trpo.common;

public interface PersistentList<T> {
    int size();

    T get(int index);

    PersistentList<T> add(int index, T value);

    PersistentList<T> add(T value);

    PersistentList<T> set(int index, T value);

    PersistentList<T> remove(int index);
}
