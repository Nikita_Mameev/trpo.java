package ru.nsu.trpo.common;

/**
 * Счётчик версий
 * При создании абсолютно новой коллекции создаётся новый.
 * При создании коллекций по изменениям - инкрементируется, но на все версии одной коллекции существует единственный экземпляр счётчика
 */
public class VersionCounter {
    private int version;

    public int getNext(){
        version += 1;
        return version;
    }

    public boolean isLast(int version){
        return this.version == version;
    }
}
