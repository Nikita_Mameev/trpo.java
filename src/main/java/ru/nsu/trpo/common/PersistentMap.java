package ru.nsu.trpo.common;

public interface PersistentMap<K, V> {
    int size();

    V get(K key);

    PersistentMap<K, V> put(K key, V value);

    PersistentMap<K, V> remove(K key);
}
