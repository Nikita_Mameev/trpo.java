package ru.nsu.trpo.tree.map;

import ru.nsu.trpo.tree.map.model.FatNode;
import ru.nsu.trpo.tree.map.model.TreeNode;

import java.util.Set;
import java.util.TreeSet;

import static java.lang.Math.max;

/**
 * Самобалансирующееся дерево поиска
 *
 * @implNote Удаления ноды из дерева нет т.к. в контексте версионируемой мапы наличие / отсутствие элемента в мапе
 *           определяется на уровне {@link FatNode} (что в фат-ноде для заданной версии значение == / != null)
 */
class AvlTree<K extends Comparable<? super K>, V> {
    private TreeNode<K, V> root = null;

    public void createNodeIfNotExists(K key) {
        if (containsKey(key)) {
            return;
        }

        TreeNode<K, V> newNode = new TreeNode<>();
        newNode.setKey(key);
        newNode.setData(new FatNode<>());
        newNode.setHeight(1);

        // Первый элемент просто ставим в корень
        if (root == null) {
            root = newNode;
            return;
        }

        // Вставка элемента
        // Ищем родителя, к которому необходимо будет прикрепить новый элемент
        TreeNode<K, V> parent = root;
        boolean lessThanParent = parent.getKey().compareTo(key) > 0;
        while (lessThanParent && parent.getLeft() != null || parent.getRight() != null) {
            parent = (lessThanParent) ? parent.getLeft() : parent.getRight();
            lessThanParent = parent.getKey().compareTo(key) > 0;
        }

        // Связываем родителя с новым узлом
        newNode.setParent(parent);

        if (lessThanParent) {
            parent.setLeft(newNode);
        } else {
            parent.setRight(newNode);
        }

        fixHeight(parent);

        // После вставки может потребоваться балансировка
        while (parent != null) {
            TreeNode<K, V> parentOfParent = parent.getParent();

            if (parentOfParent == null) {
                root = balance(parent);
                parent = null;
            } else {
                boolean isCurrentLeft = parentOfParent.getLeft() == parent;

                TreeNode<K, V> newSubtreeRoot = balance(parent);

                if (isCurrentLeft) {
                    parentOfParent.setLeft(newSubtreeRoot);
                } else {
                    parentOfParent.setRight(newSubtreeRoot);
                }

                parent = newSubtreeRoot.getParent();
            }
        }
    }

    /**
     * Балансирует поддерево, начинающееся с переданного узла, если это необходимо (если одно из его поддеревьев выше другого на 2)
     *
     * @return Новый корень поддерева
     */
    private TreeNode<K, V> balance(TreeNode<K, V> subtreeRoot) {
        fixHeight(subtreeRoot);
        int balanceFactor = fetchBalanceFactor(subtreeRoot);

        if (balanceFactor == 2) {
            if (fetchBalanceFactor(subtreeRoot.getRight()) < 0) {
                subtreeRoot.setRight(rotateRight(subtreeRoot.getRight()));
            } else {
                return rotateLeft(subtreeRoot);
            }
        } else if (balanceFactor == -2) {
            if (fetchBalanceFactor(subtreeRoot.getLeft()) > 0) {
                subtreeRoot.setLeft(rotateLeft(subtreeRoot.getLeft()));
            } else {
                return rotateRight(subtreeRoot);
            }
        }

        return subtreeRoot;
    }

    private TreeNode<K, V> rotateLeft(TreeNode<K, V> subtreeRoot) {
        TreeNode<K, V> right = subtreeRoot.getRight();
        subtreeRoot.setRight(right.getLeft());
        right.setLeft(subtreeRoot);

        right.setParent(subtreeRoot.getParent());
        subtreeRoot.setParent(right);

        fixHeight(subtreeRoot);
        fixHeight(right);

        return right;
    }

    private TreeNode<K, V> rotateRight(TreeNode<K, V> subtreeRoot) {
        TreeNode<K, V> left = subtreeRoot.getLeft();
        subtreeRoot.setLeft(left.getRight());
        left.setRight(subtreeRoot);

        left.setParent(subtreeRoot.getParent());
        subtreeRoot.setParent(left);

        fixHeight(subtreeRoot);
        fixHeight(left);

        return left;
    }

    private int fetchHeight(TreeNode<K, V> treeNode) {
        return treeNode == null ? 0 : treeNode.getHeight();
    }

    private int fetchBalanceFactor(TreeNode<K, V> treeNode) {
        return fetchHeight(treeNode.getRight()) - fetchHeight(treeNode.getLeft());
    }

    private void fixHeight(TreeNode<K, V> treeNode) {
        int leftHeight = fetchHeight(treeNode.getLeft());
        int rightHeight = fetchHeight(treeNode.getRight());
        treeNode.setHeight(max(leftHeight, rightHeight) + 1);
    }

    public FatNode<V> get(K key) {
        if (root == null) {
            return null;
        }

        return findNode(root, key);
    }

    private FatNode<V> findNode(TreeNode<K, V> treeNode, K key) {
        if (treeNode == null) {
            return null;
        }

        int keyComparision = treeNode.getKey().compareTo(key);

        if (keyComparision == 0) {
            return treeNode.getData();
        } else if (keyComparision > 0) {
            return findNode(treeNode.getLeft(), key);
        } else {
            return findNode(treeNode.getRight(), key);
        }
    }

    public boolean containsKey(K key) {
        return get(key) != null;
    }

    public Set<K> keySet() {
        Set<K> keys = new TreeSet<>();

        if (root != null) {
            fillKeySet(keys, root);
        }

        return keys;
    }

    private void fillKeySet(Set<K> keys, TreeNode<K, V> treeNode) {
        if (treeNode == null) {
            return;
        }

        keys.add(treeNode.getKey());

        fillKeySet(keys, treeNode.getLeft());
        fillKeySet(keys, treeNode.getRight());
    }
}
