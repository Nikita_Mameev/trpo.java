package ru.nsu.trpo.tree.map.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Состояние узла на момент версии {@link FatNodeState#version}
 */
@Getter
@Setter
@ToString(of = {"version", "value"})
@RequiredArgsConstructor
public class FatNodeState<T> {
    private final int version;
    private final T value;
}
