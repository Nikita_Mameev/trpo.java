package ru.nsu.trpo.tree.map;

import lombok.RequiredArgsConstructor;
import ru.nsu.trpo.common.PersistentMap;
import ru.nsu.trpo.common.VersionCounter;
import ru.nsu.trpo.tree.map.model.FatNode;
import ru.nsu.trpo.tree.map.model.FatNodeState;

@RequiredArgsConstructor
public class PersistentTreeMap<K extends Comparable<? super K>, V> implements PersistentMap<K, V> {
    private final int currentVersion;
    private final VersionCounter versionCounter;
    private final int size;
    private final AvlTree<K, V> history;

    public PersistentTreeMap() {
        this(0, new VersionCounter(), 0, new AvlTree<>());
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public V get(K key) {
        FatNode<V> node = history.get(key);

        if (node == null) {
            return null;
        }

        return node.getState(currentVersion).getValue();
    }

    @Override
    public PersistentTreeMap<K, V> put(K key, V value) {
        if (history.containsKey(key) &&
                history.get(key).getState(currentVersion).getValue() != null &&
                history.get(key).getState(currentVersion).getValue().equals(value)) {
            return this;
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().put(key, value);
        }

        int newVersion = versionCounter.getNext();
        int newSize = size;
        if (history.get(key) == null || history.get(key).getState(currentVersion) == null) {
            newSize = size + 1;
        }

        history.createNodeIfNotExists(key);
        history.get(key).addState(new FatNodeState<>(newVersion, value));

        return new PersistentTreeMap<>(newVersion, versionCounter, newSize, history);
    }

    @Override
    public PersistentTreeMap<K, V> remove(K key) {
        if (!history.containsKey(key) || history.get(key).getState(currentVersion).getValue() == null) {
            return this;
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().remove(key);
        }

        int newVersion = versionCounter.getNext();
        int newSize = size - 1;

        history.get(key).addState(new FatNodeState<>(newVersion, null));

        return new PersistentTreeMap<>(newVersion, versionCounter, newSize, history);
    }

    private PersistentTreeMap<K, V> copyFullState() {
        int copyVersion = versionCounter.getNext();

        for (K key : history.keySet()) {
            FatNode<V> node = history.get(key);
            V value = node.getState(currentVersion).getValue();
            node.addState(new FatNodeState<>(copyVersion, value));
        }

        return new PersistentTreeMap<>(copyVersion, versionCounter, size, history);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("{");

        for (K key : history.keySet()) {
            FatNode<V> node = history.get(key);
            V value = node.getState(currentVersion).getValue();

            if (value == null) {
                continue;
            }

            stringBuilder.append(String.format("%s : %s; ", key, value));
        }

        stringBuilder.append("}");

        return stringBuilder.toString();
    }
}
