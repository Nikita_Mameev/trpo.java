package ru.nsu.trpo.tree.map.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(of = {"key", "height", "left", "right"})
public class TreeNode<K extends Comparable<? super K>, V> {
    private K key;
    private int height;
    private TreeNode<K, V> left, right, parent;
    private FatNode<V> data;
}