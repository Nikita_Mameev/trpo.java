package ru.nsu.trpo;

import ru.nsu.trpo.array.list.PersistentArrayList;
import ru.nsu.trpo.common.PersistentList;

import java.util.List;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        PersistentList<Integer> a = new PersistentArrayList<>();
        PersistentList<Integer> b = a.add(1);
        PersistentList<Integer> c = b.add(3);

        PersistentList<Integer> d = c.add(1, 5);
        PersistentList<Integer> e = c.add(1, 100);

        PersistentList<Integer> f = d.set(2, 55);
        PersistentList<Integer> g = e.set(2, 100100);

        PersistentList<Integer> h = f.remove(0);
        PersistentList<Integer> i = g.remove(2);

        List<PersistentList<Integer>> lists = asList(a, b, c, d, e, f, g, h, i);
        lists.forEach(System.out::println);
    }
}
