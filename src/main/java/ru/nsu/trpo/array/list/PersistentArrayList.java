package ru.nsu.trpo.array.list;

import lombok.AllArgsConstructor;
import ru.nsu.trpo.array.list.model.FatNode;
import ru.nsu.trpo.array.list.model.FatNodeState;
import ru.nsu.trpo.common.PersistentList;
import ru.nsu.trpo.common.VersionCounter;

import java.util.Arrays;

@AllArgsConstructor
public class PersistentArrayList<T> implements PersistentList<T> {
    private final int currentVersion;
    private final VersionCounter versionCounter;
    private final int size;
    private final FatNode[] history;

    public PersistentArrayList() {
        this(0, new VersionCounter(), 0, new FatNode[2]);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        return (T) history[index].getState(currentVersion).getValue();
    }

    @Override
    public PersistentArrayList<T> add(int index, T value) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().add(index, value);
        }

        int newSize = size + 1;
        int newVersion = versionCounter.getNext();

        FatNode[] newHistory = history;
        if (newSize > history.length) {
            newHistory = Arrays.copyOf(history, history.length * 2);
        }

        if (newHistory[newSize - 1] == null) {
            newHistory[newSize - 1] = new FatNode();
        }

        newHistory[index].addState(new FatNodeState(newVersion, value));
        for (int i = index; i < size; ++i) {
            newHistory[i + 1].addState(new FatNodeState(newVersion, history[i].getState(currentVersion).getValue()));
        }

        return new PersistentArrayList<>(newVersion, versionCounter, newSize, newHistory);
    }

    @Override
    public PersistentArrayList<T> add(T value) {
        return add(size(), value);
    }

    @Override
    public PersistentArrayList<T> set(int index, T value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().set(index, value);
        }

        int newVersion = versionCounter.getNext();
        history[index].addState(new FatNodeState(newVersion, value));

        return new PersistentArrayList<>(newVersion, versionCounter, size, history);
    }

    @Override
    public PersistentArrayList<T> remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().remove(index);
        }

        int newSize = size - 1;
        int newVersion = versionCounter.getNext();

        for (int i = index; i < size - 1; ++i) {
            history[i].addState(new FatNodeState(newVersion, history[i + 1].getState(currentVersion).getValue()));
        }

        return new PersistentArrayList<>(newVersion, versionCounter, newSize, history);
    }

    private PersistentArrayList<T> copyFullState() {
        int copyVersion = versionCounter.getNext();

        for (int i = 0; i < size; ++i) {
            FatNode node = history[i];
            Object value = node.getState(currentVersion).getValue();
            node.addState(new FatNodeState(copyVersion, value));
        }

        return new PersistentArrayList<>(copyVersion, versionCounter, size, history);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("[");

        for (int i = 0; i < size; ++i) {
            stringBuilder.append(history[i].getState(currentVersion).getValue());

            if (i != size - 1) {
                stringBuilder.append(", ");
            }
        }

        stringBuilder.append("]");

        return stringBuilder.toString();
    }
}
