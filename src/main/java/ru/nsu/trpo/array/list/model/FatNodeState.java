package ru.nsu.trpo.array.list.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Состояние узла на момент версии {@link FatNodeState#version}
 */
@Getter
@Setter
@ToString(of = {"version", "value"})
@RequiredArgsConstructor
public class FatNodeState {
    private final int version;
    private final Object value;
}
