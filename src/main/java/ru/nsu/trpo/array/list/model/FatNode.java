package ru.nsu.trpo.array.list.model;

import lombok.ToString;
import ru.nsu.trpo.common.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Узел, содержащий всю историю версий
 */
@ToString
public class FatNode {
    private final List<FatNodeState> history = new ArrayList<>();

    public void addState(FatNodeState newState) {
        history.add(newState);
    }

    public FatNodeState getState(int version) {
        FatNodeState state = Utils.findStateOfVersion(history, version, FatNodeState::getVersion);

        if (state == null) {
            return new FatNodeState(version, null);
        }

        return state;
    }
}
