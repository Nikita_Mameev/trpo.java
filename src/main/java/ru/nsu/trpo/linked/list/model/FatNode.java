package ru.nsu.trpo.linked.list.model;

import lombok.ToString;
import ru.nsu.trpo.common.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Узел, содержащий всю историю версий
 */
@ToString
public class FatNode<T> {
    private final List<FatNodeState<T>> history = new ArrayList<>();

    public void addState(FatNodeState<T> newState) {
        history.add(newState);
    }

    public FatNodeState<T> getState(int version) {
        FatNodeState<T> state = Utils.findStateOfVersion(history, version, FatNodeState::getVersion);

        if (state == null) {
            throw new IndexOutOfBoundsException(String.format("There is no %d state in history: %s", version, history));
        }

        return state;
    }
}
