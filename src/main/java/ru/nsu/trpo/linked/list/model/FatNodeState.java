package ru.nsu.trpo.linked.list.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Состояние узла на момент версии {@link FatNodeState#version}
 */
@Getter
@Setter
@ToString(of = {"version", "value"})
@RequiredArgsConstructor
public class FatNodeState<T> {
    private final int version;
    private final T value;
    private final FatNode<T> prev;
    private final FatNode<T> next;
}
