package ru.nsu.trpo.linked.list;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import ru.nsu.trpo.array.list.PersistentArrayList;
import ru.nsu.trpo.common.PersistentList;
import ru.nsu.trpo.common.VersionCounter;
import ru.nsu.trpo.linked.list.model.FatNode;
import ru.nsu.trpo.linked.list.model.FatNodeState;


@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class PersistentLinkedList<T> implements PersistentList<T> {
    private final int currentVersion;
    private final VersionCounter versionCounter;
    private final int size;

    private final FatNode<T> head;
    private final FatNode<T> tail;

    public PersistentLinkedList() {
        this(0, new VersionCounter(), 0, null, null);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (index == size - 1) {
            return tail.getState(currentVersion).getValue();
        }

        return getNode(index).getState(currentVersion).getValue();
    }

    @Override
    public PersistentLinkedList<T> add(T value) {
        return add(size(), value);
    }

    @Override
    public PersistentLinkedList<T> add(int index, T value) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().add(index, value);
        }

        int newSize = size + 1;
        int newVersion = versionCounter.getNext();
        FatNode<T> newNode = new FatNode<>();

        if (size == 0) {
            newNode.addState(new FatNodeState<>(newVersion, value, null, null));
            return new PersistentLinkedList<>(newVersion, versionCounter, newSize, newNode, newNode);
        } else if (index == 0) {
            newNode.addState(new FatNodeState<>(newVersion, value, null, head));
            head.addState(new FatNodeState<>(newVersion, head.getState(currentVersion).getValue(), newNode, head.getState(currentVersion).getNext()));
            return new PersistentLinkedList<>(newVersion, versionCounter, newSize, newNode, tail);
        } else if (index == size) {
            newNode.addState(new FatNodeState<>(newVersion, value, tail, null));
            tail.addState(new FatNodeState<>(newVersion, tail.getState(currentVersion).getValue(), tail.getState(currentVersion).getPrev(), newNode));
            return new PersistentLinkedList<>(newVersion, versionCounter, newSize, head, newNode);
        }

        // Если это вставка в середину, то находим узел, на место которого необходимо поместить текущий
        // Этот узел после вставки сместится вправо
        FatNode<T> rightNode = getNode(index);
        FatNodeState<T> rightNodeState = rightNode.getState(currentVersion);
        FatNode<T> leftNode = rightNodeState.getPrev();
        FatNodeState<T> leftNodeState = leftNode.getState(currentVersion);

        newNode.addState(new FatNodeState<>(newVersion, value, leftNode, rightNode));
        rightNode.addState(new FatNodeState<>(newVersion, rightNodeState.getValue(), newNode, rightNodeState.getNext()));
        leftNode.addState(new FatNodeState<>(newVersion, leftNodeState.getValue(), leftNodeState.getPrev(), newNode));

        return new PersistentLinkedList<>(newVersion, versionCounter, newSize, head, tail);
    }

    @Override
    public PersistentLinkedList<T> set(int index, T value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().set(index, value);
        }

        int newVersion = versionCounter.getNext();
        FatNode<T> node = getNode(index);
        FatNodeState<T> nodeState = node.getState(currentVersion);
        node.addState(new FatNodeState<>(newVersion, value, nodeState.getPrev(), nodeState.getNext()));

        return new PersistentLinkedList<>(newVersion, versionCounter, size, head, tail);
    }

    @Override
    public PersistentLinkedList<T> remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        if (!versionCounter.isLast(currentVersion)) {
            return copyFullState().remove(index);
        }

        int newSize = size - 1;
        int newVersion = versionCounter.getNext();

        if (size == 1) {
            return new PersistentLinkedList<>(newVersion, versionCounter, newSize, null, null);
        } else if (index == 0) {
            FatNode<T> newHead = head.getState(currentVersion).getNext();
            FatNodeState<T> newHeadState = newHead.getState(currentVersion);
            newHead.addState(new FatNodeState<>(newVersion, newHeadState.getValue(), null, newHeadState.getNext()));
            return new PersistentLinkedList<>(newVersion, versionCounter, newSize, newHead, tail);
        } else if (index == size - 1) {
            FatNode<T> newTail = tail.getState(currentVersion).getPrev();
            FatNodeState<T> newTailState = newTail.getState(currentVersion);
            newTail.addState(new FatNodeState<>(newVersion, newTailState.getValue(), newTailState.getPrev(), null));
            return new PersistentLinkedList<>(newVersion, versionCounter, newSize, head, newTail);
        }

        FatNode<T> currentNode = getNode(index);
        FatNodeState<T> currentNodeState = currentNode.getState(currentVersion);

        // Если это удаление из середины, то склеим соседние узлы
        // Этот узел после вставки сместится вправо
        FatNode<T> rightNode = currentNodeState.getNext();
        FatNode<T> leftNode = currentNodeState.getPrev();
        FatNodeState<T> rightNodeState = rightNode.getState(currentVersion);
        FatNodeState<T> leftNodeState = leftNode.getState(currentVersion);

        rightNode.addState(new FatNodeState<>(newVersion, rightNodeState.getValue(), leftNode, rightNodeState.getNext()));
        leftNode.addState(new FatNodeState<>(newVersion, leftNodeState.getValue(), leftNodeState.getPrev(), rightNode));

        return new PersistentLinkedList<>(newVersion, versionCounter, newSize, head, tail);
    }

    private PersistentLinkedList<T> copyFullState() {
        int copyVersion = versionCounter.getNext();

        FatNode<T> iterator = head;
        while (iterator != null) {
            FatNodeState<T> currentVersionState = iterator.getState(currentVersion);
            iterator.addState(new FatNodeState<>(copyVersion, currentVersionState.getValue(), currentVersionState.getPrev(), currentVersionState.getNext()));
            iterator = iterator.getState(currentVersion).getNext();
        }

        return new PersistentLinkedList<>(copyVersion, versionCounter, size, head, tail);
    }

    private FatNode<T> getNode(int index) {
        FatNode<T> iterator = head;
        for (int i = 0; i < index; i++) {
            iterator = iterator.getState(currentVersion).getNext();
        }

        return iterator;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("[");

        FatNode<T> iterator = head;
        while (iterator != null) {
            stringBuilder.append(iterator.getState(currentVersion).getValue());

            if (iterator.getState(currentVersion).getNext() != null) {
                stringBuilder.append(", ");
            }

            iterator = iterator.getState(currentVersion).getNext();
        }

        stringBuilder.append("]");

        return stringBuilder.toString();
    }
}
